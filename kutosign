#!/bin/bash
# 
# Copyright © 2020 Kutometa SPC, Kuwait
#
# All rights reserved. Unless explicitly stated otherwise, this program 
# is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE 
# WARRANTY OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
# See accompanying documentation for more details.
#
# www.ka.com.kw

VERSION=0.5
DOC='USAGE

    '"$(basename "$0")"' [ verify [ GPG-HOMEDIR [ KEYID ] ] ]
    '"$(basename "$0")"' sign GPG-HOMEDIR KEYID

This tool generates and verifies signatures of directory content. 

When invoked without any arguments, this tool looks for 
`signatures.*` files in the current directory and verifies any files 
found against their detached signatures using your users default GPG 
keyring.

Once verified, the hashes in these files are verified against each
file under the current directory (aborting with an error if the file 
has no associated hash, has a different hash, or if the file does 
not exist).

When invoked with the `sign` command, the following files are 
generated:

    signatures.sha256  
    signatures.sha384  
    signatures.sha256.asc  
    signatures.sha384.asc
    
signatures.{HASH} files are in a format that is compatible with 
standard {HASH}sum utilities. signatures.{HASH}.asc files are 
standard detached pgp signatures.


DEPENDENCIES
    
    gpg(1)
    gpgv(1)
    sha256sum(1)
    sha384sum(1)
    coreutls


COMMANDS
    
    (None) or verify    Verify hashes and signatures in current 
                        directory
            
    sign                Generate hashes and signatures
    
    append              Append another signature
    
    help                Show this help message
    
    version             Show tool version


BUGS AND SOURCE CODE

The source code of this project is maintained in a git repository 
at code.ka.com.kw. Bug reports and features request are welcome 
there. You can visit this repository at:

    https://code.ka.com.kw/miscellaneous/kutosign


COPYRIGHT

(ح) حقوق المؤلف محفوظة لشركة كوتوميتا لبرمجة وتشغيل الكمبيوتر وتصميم 
وإدارة مواقع الإنترنت (ش.ش.و) - سنة ٢٠٢٠

تنفي الشركة أي مسئولية عن عدم أهلية البرنامج لأداء وظيفته المعلن عنها 
أو عن الأضرار التي قد يتكبدها المستخدم وغيره نتيجة استخدام هذا 
البرنامج. تقع مسؤولية أي ضرر ناجم عن استخدام هذا البرنامج على عاتق 
المستخدم وحده. اطلع على المستندات المرافقة لهذا البرنامج لمزيد من 
المعلومات.

Copyright © 2020 Kutometa SPC, Kuwait
All rights reserved. Unless explicitly stated otherwise, this program 
is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE 
WARRANTY OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
See accompanying documentation for more details.


Kutometa SPC
contact@ka.com.kw
www.ka.com.kw'

ARG1="$1"
ARG2="$2"
ARG3="$3"
set -euH
gen_sha256_sum() {
    find -L . -type f \
              -not -path './.git/*' \
              -not -path './signatures.sha256' \
              -not -path './signatures.sha384' \
              -not -path './signatures.sha256.*.asc' \
              -not -path './signatures.sha384.*.asc' \
              -exec sha256sum {} \; | env LC_ALL=C sort -k 2
}

gen_sha384_sum() {
    find -L . -type f \
              -not -path './.git/*' \
              -not -path './signatures.sha256' \
              -not -path './signatures.sha384' \
              -not -path './signatures.sha256.*.asc' \
              -not -path './signatures.sha384.*.asc' \
              -exec sha384sum {} \; | env LC_ALL=C sort -k 2
}
sign() {
    gpg --homedir "$1" \
        --detach-sign \
        --armor \
        --batch \
        --yes \
        -o - \
        "${EXTRA_ARGS[@]}" "$2"
}

if [[ "$ARG1" == "sign" ]]; then
    if ! [[ -d "$ARG2" ]]; then 
        echo "GPG home directory '$ARG2' does not exist or is not a directory."
        exit 1
    fi
    
    if ! [[ "$ARG3" ]]; then
        echo "GPG key id was not given."
        gpg --homedir "$ARG2" --list-keys
        exit 1
    fi
    
    if ! gpg --homedir "$ARG2" --list-keys "$ARG3" 2>/dev/null >&2; then
        echo "No gpg key with ID '$ARG3' found in '$ARG2'."
        exit 1
    fi
    
    EXTRA_ARGS=("--local-user" "$ARG3")
    
    if [[ -f "signatures.sha256" ]]; then
        for i in signatures.sha256*; do 
            echo "* Removing $i"
            rm "$i"
        done
    fi
    
    if [[ -f "signatures.sha384" ]]; then
        for i in signatures.sha384*; do 
            echo "* Removing $i"
            rm "$i"
        done
    fi
    
    echo "* Generating SHA256 digests: signatures.sha256"
    gen_sha256_sum > signatures.sha256
    echo "* Generating SHA384 digests: signatures.sha384"
    gen_sha384_sum > signatures.sha384
    echo "* Singing SHA256 digests: signatures.sha256.$ARG3.asc"
    sign "$ARG2" "signatures.sha256" > "signatures.sha256.$ARG3.asc"
    echo "* Singing SHA384 digests: signatures.sha384.$ARG3.asc"
    sign "$ARG2" "signatures.sha384" > "signatures.sha384.$ARG3.asc"
    exit 0
elif [[ "$ARG1" == "append" ]];then
    if ! [[ -d "$ARG2" ]]; then 
        echo "GPG home directory '$ARG2' does not exist or is not a directory."
        exit 1
    fi
    
    if ! [[ "$ARG3" ]]; then
        echo "GPG key id was not given."
        exit 1
    fi
    
    if ! gpg --homedir "$ARG2" --list-keys "$ARG3" 2>/dev/null >&2; then
        echo "No gpg key with ID '$ARG3' found in '$ARG2'."
        exit 1
    fi
    
    EXTRA_ARGS=("--local-user" "$ARG3")
    echo "* Singing SHA256 digests: signatures.sha256.$ARG3.asc"
    sign "$ARG2" "signatures.sha256" > "signatures.sha256.$ARG3.asc"
    echo "* Singing SHA384 digests: signatures.sha384.$ARG3.asc"
    sign "$ARG2" "signatures.sha384" > "signatures.sha384.$ARG3.asc"
    exit 0
elif [[ "$ARG1" == "help" ]] || [[ "$ARG1" == "--help" ]]; then
    echo -n "$DOC" | less
elif [[ "$ARG1" == "version" ]] || [[ "$ARG1" == "--version" ]]; then
    echo "$VERSION"
elif [[ "$ARG1" == "" ]] || [[ "$ARG1" == "verify" ]]; then
    GPG_CMD=()
    if [[ "$ARG2" ]]; then
        if ! [[ -d "$ARG2" ]]; then
            printf "\e[31;1mThe gpg home directory '$ARG2' does not exist.\e[0m\n"
            exit 1
        fi
        GPG_CMD=("gpg" "--homedir" "$ARG2")
    else
        GPG_CMD=("gpg")
    fi
    
    # build gpg command to verify files
    TEMP_KEYRING=""
    TEMP_GPG_DIR=""
    if [[ "$ARG3" ]]; then
        if ! gpg --homedir "$ARG2" --list-keys "$ARG3" 2>/dev/null >&2; then
            printf "\e[31;1mNo gpg key with ID '$ARG3' found in '$ARG2'.\e[0m\n"
            exit 1
        fi
        TEMP_KEYRING="$(mktemp)"
        TEMP_GPG_DIR="$(mktemp -d)"
        gpg --homedir "$ARG2" --export "$ARG3" > "$TEMP_KEYRING"
        GPG_CMD=( "gpgv" "--quiet" "--homedir" "$TEMP_GPG_DIR" "--keyring" "$TEMP_KEYRING" )
    else
        GPG_CMD=( "${GPG_CMD[@]}" "--verify" )
    fi
    
    # check all sha384 signatures and make sure that at least one signature passes
    COUNT="0"
    OVERALL="0"
    for signature in signatures.sha256.*.asc; do
        printf "\e[2m"
        (( ++OVERALL )) || true
        if "${GPG_CMD[@]}" "$signature" "signatures.sha256"; then 
            (( ++COUNT )) || true
        fi
        printf "\e[0m\n"
    done
    if [[ "$COUNT" -lt 1 ]]; then
        printf "\e[31;1m* $COUNT SHA256 gpg signature(s) out of $OVERALL is/are valid. Aborting.\e[0m\n"
        exit 1
    else
        printf "\e[32;1m* $COUNT SHA256 gpg signature(s) out of $OVERALL is/are valid.\e[0m\n"
    fi
    
    # check all sha384 signatures and make sure that at least one signature passes
    COUNT="0"
    OVERALL="0"
    for signature in signatures.sha384.*.asc; do
        printf "\e[2m"
        (( ++OVERALL )) || true
        if "${GPG_CMD[@]}" "$signature" "signatures.sha384"; then
            (( ++COUNT )) || true
        fi
        printf "\e[0m\n"
    done
    if [[ "$COUNT" -lt 1 ]]; then
        printf "\e[31;1m* $COUNT SHA384 gpg signature(s) out of $OVERALL is/are valid. Aborting.\e[0m\n"
        exit 1
    else
        printf "\e[32;1m* $COUNT SHA384 gpg signature(s) out of $OVERALL is/are valid.\e[0m\n"
    fi
    
    # remove temporary files
    if [[ -f "$TEMP_KEYRING" ]]; then
        rm "$TEMP_KEYRING"
    fi
    if [[ -f "$TEMP_GPG_DIR" ]]; then
        rm -r "$TEMP_GPG_DIR"
    fi
    
    # check hashes
    SIGNED_SHA256_SUMS=$(cat signatures.sha256)
    SIGNED_SHA384_SUMS=$(cat signatures.sha384)
    
    REAL_SHA256_SUMS=$(gen_sha256_sum)
    REAL_SHA384_SUMS=$(gen_sha384_sum)
    
    if [[ "$REAL_SHA256_SUMS" != "$SIGNED_SHA256_SUMS" ]]; then
        printf "\e[31;1mSHA256 hash mismatch.\e[0m\n"
        if sha256sum --quiet -c signatures.sha256; then
            diff <(echo "$REAL_SHA256_SUMS") <(echo "$SIGNED_SHA256_SUMS")
            printf "\e[31;1mThe number of SHA256 files in this directory differs from the number digests available. Aborting.\e[0m\n"
        fi
        exit 1
    fi
    
    if [[ "$REAL_SHA384_SUMS" != "$SIGNED_SHA384_SUMS" ]]; then
        printf "\e[31;1mSHA384 hash mismatch.\e[0m\n"
        if sha384sum --quiet -c signatures.sha384; then
            diff <(echo "$REAL_SHA384_SUMS") <(echo "$SIGNED_SHA384_SUMS")
            printf "\e[31;1mThe number of SHA384 files in this directory differs from the number digests available. Aborting.\e[0m\n"
        fi
        exit 1
    fi
    printf "\e[32;1m* All directory files are accounted for and match their signed checksum.\e[0m\n"
    exit 0
else
    echo "Unknown operation '$ARG1'"
    exit 1
fi
exit 1
