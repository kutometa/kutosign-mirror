USAGE

    kutosign [ verify [ GPG-HOMEDIR [ KEYID ] ] ]
    kutosign sign GPG-HOMEDIR KEYID

This tool generates and verifies signatures of directory content. 

When invoked without any arguments, this tool looks for 
`signatures.*` files in the current directory and verifies any files 
found against their detached signatures using your users default GPG 
keyring.

Once verified, the hashes in these files are verified against each
file under the current directory (aborting with an error if the file 
has no associated hash, has a different hash, or if the file does 
not exist).

When invoked with the `sign` command, the following files are 
generated:

    signatures.sha256  
    signatures.sha384  
    signatures.sha256.asc  
    signatures.sha384.asc
    
signatures.{HASH} files are in a format that is compatible with 
standard {HASH}sum utilities. signatures.{HASH}.asc files are 
standard detached pgp signatures.


DEPENDENCIES
    
    gpg(1)
    gpgv(1)
    sha256sum(1)
    sha384sum(1)
    coreutls


COMMANDS
    
    (None) or verify    Verify hashes and signatures in current 
                        directory
            
    sign                Generate hashes and signatures
    
    append              Append another signature
    
    help                Show this help message
    
    version             Show tool version


BUGS AND SOURCE CODE

The source code of this project is maintained in a git repository 
at code.ka.com.kw. Bug reports and features request are welcome 
there. You can visit this repository at:

    https://code.ka.com.kw/miscellaneous/kutosign


COPYRIGHT

(ح) حقوق المؤلف محفوظة لشركة كوتوميتا لبرمجة وتشغيل الكمبيوتر وتصميم 
وإدارة مواقع الإنترنت (ش.ش.و) - سنة ٢٠٢٠

تنفي الشركة أي مسئولية عن عدم أهلية البرنامج لأداء وظيفته المعلن عنها 
أو عن الأضرار التي قد يتكبدها المستخدم وغيره نتيجة استخدام هذا 
البرنامج. تقع مسؤولية أي ضرر ناجم عن استخدام هذا البرنامج على عاتق 
المستخدم وحده. اطلع على المستندات المرافقة لهذا البرنامج لمزيد من 
المعلومات.

Copyright © 2020 Kutometa SPC, Kuwait
All rights reserved. Unless explicitly stated otherwise, this program 
is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE 
WARRANTY OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
See accompanying documentation for more details.


Kutometa SPC
contact@ka.com.kw
www.ka.com.kw